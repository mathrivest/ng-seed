'use strict';

var config = require('../config').sass,
    browserSync = require('browser-sync'),
    gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    //cmq = require('gulp-combine-media-queries'),
    rename = require('gulp-rename'),
    cssmin = require('gulp-minify-css');


var sassTask = function (min) {
    var task = gulp.src(config.src)
        .pipe(sourcemaps.init())
        .pipe(sass(config.sass))
        //.pipe(cmq(config.cmq))
        .pipe(autoprefixer(config.autoprefixer));

    if (min) {
        task
            .pipe(rename(config.minified))
            .pipe(cssmin(config.cssmin))
            .pipe(sourcemaps.write(config.sourcemaps.dest))
            .pipe(gulp.dest(config.dest));
    } else {
        task
            .pipe(sourcemaps.write(config.sourcemaps.dest))
            .pipe(gulp.dest(config.dest));
    }

    task.pipe(browserSync.reload({
        stream: true
    }));
};

gulp.task('sass', function () {
    var min = false;

    sassTask(min);
});

module.exports = sassTask;
