'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence');

/**
 * Run local server with filesystem watch running on dev environment
 */
gulp.task('default', ['install'], function() {
    return runSequence(
        'clean',
        'sass',
        ['assets:fonts', 'assets:images', 'assets:html'],
        'watch'
    )
});
