'use strict';

var gulp = require('gulp'),
    runSequence = require('run-sequence');

/**
 * Build assets
 */
gulp.task('build', function() {
    return runSequence(
        'clean',
        'sass',
        ['assets:fonts', 'assets:images', 'assets:html']
    )
});

gulp.task('native', ['install', 'build']);
