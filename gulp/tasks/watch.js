'use strict';

var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    config = require('../config');

gulp.task('watch', function () {

    gulp.watch(config.layout.src, ['assets:html'])
        .on('change', browserSync.reload);

    gulp.watch(config.sass.files, ['sass']);

    return browserSync(config.browserSync);
});
