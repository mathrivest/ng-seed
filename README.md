# NG SEED

## Install Required Dependencies

### NodeJS
[http://nodejs.org/](http://nodejs.org/)

**Make sure to select features**

- npm package manager
- add to PATH
- Install NVM (```curl https://raw.githubusercontent.com/creationix/nvm/v0.24.0/install.sh | bash```)

** Test NodeJS installation**

Running 
```
node -v
``` 
in the command prompt should give you the installed NodeJS version.

### E2E (Protractor)
To run in command line
```sudo npm install protractor -g```
```sudo webdriver-manager update```
```protractor test/e2e/config.js --suite nameOfTestSuite```


### Node Version Manager (NVM)

todo

### GIT
**You must install GIT in your system PATH**

#### Windows
```C:\Program Files (x86)\Git\bin```

#### MAC

Add the following line to ```~/.bash_profile```

```export PATH=/usr/local/git/bin:$PATH```


### Gulp (build system)

```gulp```

## Sass
